package com.example.demo.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestErrorInfo {
    private Error error;

    public RestErrorInfo(Exception ex, int errorCode) {
        error = new Error(errorCode, ex.getLocalizedMessage());
    }

    public RestErrorInfo(String messageException, int errorCode) {
        error = new Error(errorCode, messageException);
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
