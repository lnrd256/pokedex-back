package com.example.demo.exceptions;

public class GeneralValidationException extends RuntimeException {
    public GeneralValidationException(String message) {
        super(message);
    }
}