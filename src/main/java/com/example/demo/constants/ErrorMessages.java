package com.example.demo.constants;

public enum ErrorMessages {

    GENERIC_NOT_FOUND_ERROR("generic_not_found_error"),
    GENERIC_BAD_REQUEST_ERROR("generic_bad_request_error");

    private final String message;

    ErrorMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
