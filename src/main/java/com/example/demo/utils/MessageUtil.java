package com.example.demo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageUtil {
    private final MessageSource messageSource;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Locale locale;

    @Autowired
    public MessageUtil(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String translateMessage(String message) {
        if (locale == null) {
            locale = LocaleContextHolder.getLocale();
        }
        String transMessage = message;

        try {
            transMessage = messageSource.getMessage(message, null, locale);
        } catch (NoSuchMessageException noSuchMessageException) {
        }

        return transMessage;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}