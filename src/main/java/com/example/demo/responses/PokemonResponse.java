package com.example.demo.responses;

import com.example.demo.dto.BasicPokemonDto;
import lombok.Data;

import java.util.List;

@Data
public class PokemonResponse {

    public Integer count;

    public List<BasicPokemonDto> results;
}
