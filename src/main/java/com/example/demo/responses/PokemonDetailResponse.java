package com.example.demo.responses;

import com.example.demo.dto.*;
import lombok.Data;

import java.util.List;

@Data
public class PokemonDetailResponse {

    public Integer id;

    public String name;

    public Integer weight;

    public List<TypeDto> types;

    public List<AbilityDto> abilities;

    public SpriteDto sprites;

    private List<MovesDto> moves;
}
