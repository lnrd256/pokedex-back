package com.example.demo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.example.demo.routes.Routes.HEALTH;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = HEALTH)
public class HealthCheckController {

    @GetMapping()
    public ResponseEntity<Map> health() {
        Map map = new HashMap<String, String>();
        map.put("status", "UP");

        return ok(map);
    }
}
