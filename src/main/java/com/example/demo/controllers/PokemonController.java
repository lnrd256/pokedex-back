package com.example.demo.controllers;

import com.example.demo.responses.PokemonDetailResponse;
import com.example.demo.responses.PokemonResponse;
import com.example.demo.services.PokemonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.demo.routes.Routes.DETAILS;
import static com.example.demo.routes.Routes.POKEMONS;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = POKEMONS)
public class PokemonController {

    @Autowired
    private PokemonsService pokemonsService;

    @GetMapping
    public ResponseEntity<?> getList(@RequestParam(value = "page", required = false) Integer page) {

        if (page == null) {
            page = 0;
        }

        return ResponseEntity.ok(pokemonsService.getPokemonList(page));
    }

    @GetMapping(DETAILS)
    public ResponseEntity<?> getPokemon(@PathVariable String name) {

        return ResponseEntity.ok(pokemonsService.getPokemon(name));
    }
}
