package com.example.demo.routes;

public final class Routes {

    public static final String HEALTH = "/health";
    public static final String POKEMONS = "/pokemons";
    public static final String DETAILS = "/detail/{name}";
}
