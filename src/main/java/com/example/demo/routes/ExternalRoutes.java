package com.example.demo.routes;

public final class ExternalRoutes {

    public static final String POKEAPI = "https://pokeapi.co";
    public static final String POKEMON_LIST = POKEAPI + "/api/v2/pokemon";

}
