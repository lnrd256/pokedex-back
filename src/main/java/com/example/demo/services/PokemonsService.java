package com.example.demo.services;

import com.example.demo.dto.BasicPokemonDto;
import com.example.demo.requesters.PokemonRequester;
import com.example.demo.responses.PokemonDetailResponse;
import com.example.demo.responses.PokemonResponse;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class PokemonsService {

    @Autowired
    private PokemonRequester pokemonRequester;

    public List<PokemonDetailResponse> getPokemonList(int page) {
        PokemonResponse pokemonResponse = pokemonRequester.getPokemonList(page);

        List<PokemonDetailResponse> pokemonList = new ArrayList<>();

        for (BasicPokemonDto basicPokemonDto : pokemonResponse.results) {

            PokemonDetailResponse pokemonDetail = pokemonRequester.getPokemonFromUrl(basicPokemonDto.getUrl());
            pokemonDetail.setName(basicPokemonDto.getName());
            pokemonList.add(pokemonDetail);
        }

        return pokemonList;
    }

    public PokemonDetailResponse getPokemon(String pokemon) {
        PokemonDetailResponse pokemonDetailResponse = new PokemonDetailResponse();

        pokemonDetailResponse = pokemonRequester.getPokemonFromName(pokemon);

        return pokemonDetailResponse;
    }
}
