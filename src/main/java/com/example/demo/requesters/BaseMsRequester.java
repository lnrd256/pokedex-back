package com.example.demo.requesters;

import com.example.demo.exceptions.GeneralValidationException;
import com.example.demo.exceptions.RestErrorInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

import static com.example.demo.constants.ErrorMessages.GENERIC_BAD_REQUEST_ERROR;
import static com.example.demo.constants.ErrorMessages.GENERIC_NOT_FOUND_ERROR;

public class BaseMsRequester {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected Requester requester;

    protected ObjectMapper objectMapper = new ObjectMapper();

    public void validateResponse(
            ResponseEntity response,
            Boolean throw500,
            Boolean throwResponseError
    ) throws Exception {

        throw500 = throw500 == null ? false : throw500;
        throwResponseError = throwResponseError == null ? false : throwResponseError;

        if (response.getStatusCode().is4xxClientError()) {

            if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new GeneralValidationException(GENERIC_NOT_FOUND_ERROR.getMessage());
            }

            if (throwResponseError && response.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throwMsBadRequestError(response.getBody());
            }
        }

        if (throw500 && response.getStatusCode().is5xxServerError()) {
            throw new Exception();
        }
    }

    private void throwMsBadRequestError(Object body) throws Exception {

        if (body != null) {
            logger.info("[ throwMsBadRequestError ] body = {}", objectMapper.writeValueAsString(body));

            String stringBody = body.toString();

            if (stringBody.contains("errors")) {

                throw new GeneralValidationException(getMonolithErrorMessage(stringBody));
            }

            RestErrorInfo error = objectMapper.readValue(
                    objectMapper.writeValueAsString(body),
                    RestErrorInfo.class
            );

            throw new GeneralValidationException(error.getError().getMessage());

        } else {

            throw new GeneralValidationException(GENERIC_BAD_REQUEST_ERROR.getMessage());
        }
    }

    private String getMonolithErrorMessage(String stringBody) {

        return stringBody.replaceAll("\"", "");
    }

    public String buildUrlWithParameters(String basePath, Map<String, String> params) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(basePath);

        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.queryParam(entry.getKey(), entry.getValue());
        }

        return builder.toUriString();
    }
}
