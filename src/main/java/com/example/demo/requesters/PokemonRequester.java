package com.example.demo.requesters;

import com.example.demo.responses.PokemonDetailResponse;
import com.example.demo.responses.PokemonResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import static com.example.demo.routes.ExternalRoutes.POKEMON_LIST;

@Component
public class PokemonRequester extends BaseMsRequester {

    public PokemonResponse getPokemonList(Integer page) {

        PokemonResponse result = new PokemonResponse();
        ResponseEntity<PokemonResponse> response;
        page = page * 20;

        try {
            response = requester.execute(
                    POKEMON_LIST + "?offset=" + page + "&limit=20",
                    HttpMethod.GET,
                    null,
                    PokemonResponse.class,
                    null
            );

            result = response.getBody();

        } catch (Exception e) {
            logger.error("[ notifyNewStorekeeper ] Exception: {}", e.getMessage());
        }

        return result;
    }

    public PokemonDetailResponse getPokemonFromUrl(String url) {

        PokemonDetailResponse result = new PokemonDetailResponse();
        ResponseEntity<PokemonDetailResponse> response;

        try {
            response = requester.execute(
                    url,
                    HttpMethod.GET,
                    null,
                    PokemonDetailResponse.class,
                    null
            );

            result = response.getBody();

        } catch (Exception e) {
            logger.error("[ notifyNewStorekeeper ] Exception: {}", e.getMessage());
        }

        return result;
    }

    public PokemonDetailResponse getPokemonFromName(String name) {
        PokemonDetailResponse result = new PokemonDetailResponse();
        ResponseEntity<PokemonDetailResponse> response;
        String url = POKEMON_LIST + "/" + name;

        try {
            response = requester.execute(
                    url,
                    HttpMethod.GET,
                    null,
                    PokemonDetailResponse.class,
                    null
            );

            result = response.getBody();

        } catch (Exception e) {
            logger.error("[ notifyNewStorekeeper ] Exception: {}", e.getMessage());
        }

        return result;
    }
}
