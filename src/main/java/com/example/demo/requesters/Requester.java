package com.example.demo.requesters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;

@Component
public class Requester {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    protected ObjectMapper objectMapper;

    private HttpHeaders getBasicHeaders() {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    public HttpHeaders getHeaders(HashMap<String, String> headerHashMap) {

        HttpHeaders httpHeaders = getBasicHeaders();

        if (headerHashMap != null) {

            headerHashMap.forEach(httpHeaders::set);
        }

        return httpHeaders;
    }

    public HashMap<String, String> getRedirectHeaders() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        Enumeration headerNames = request.getHeaderNames();

        HashMap<String, String> redirectHeaders = new HashMap<>();

        while (headerNames.hasMoreElements()) {

            String key = (String)headerNames.nextElement();
            String value = request.getHeader(key);

            redirectHeaders.put(key, value);
        }

        redirectHeaders.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        return redirectHeaders;
    }

    public <T> ResponseEntity<T> execute(
            String url,
            HttpMethod method,
            Object requestEntity,
            Class<T> classT,
            HashMap<String, String> headers
    ) throws Exception {

        HttpHeaders httpHeaders = this.getHeaders(headers);

        HttpEntity request = new HttpEntity(requestEntity, httpHeaders);

        ResponseEntity<T> response;

        try {

            response = restTemplate.exchange(
                    url,
                    method,
                    request,
                    classT
            );
        } catch (HttpClientErrorException ex) {

            logger.info("[ execute ] HttpClientErrorException " +
                            "> json = {}, " +
                            " > Url {},  " +
                            " > Error message {},  " +
                            " > with request body {}  " +
                            " > with method {}",
                    ex.getResponseBodyAsString(),
                    url,
                    ex.getMessage(),
                    objectMapper.writeValueAsString(requestEntity),
                    method.toString()
            );

            throw new Exception();
        } catch (Exception exception) {

            logger.error("Url {},  Error message {},  with request body {}  with method {}",
                    url,
                    exception.getMessage(),
                    objectMapper.writeValueAsString(requestEntity),
                    method.toString(),
                    exception
            );

            throw new Exception();
        }

        if (response.getStatusCode().is4xxClientError()) {
            logger.error("Url {},  Error 4xx response {},  with request body {}  with method {}",
                    url,
                    objectMapper.writeValueAsString(response.getBody()),
                    objectMapper.writeValueAsString(requestEntity),
                    method.toString()
            );
        }

        if (response.getStatusCode().is5xxServerError()) {
            logger.error("Url {},  Error 5xx response {},  with request body {}  with method {}",
                    url,
                    objectMapper.writeValueAsString(response.getBody()),
                    objectMapper.writeValueAsString(requestEntity),
                    method.toString()
            );
        }

        return response;
    }
}
