package com.example.demo.dto;

import lombok.Data;

@Data
public class MoveDto {

    String name;

    String url;
}
