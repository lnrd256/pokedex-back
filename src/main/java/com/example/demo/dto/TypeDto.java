package com.example.demo.dto;

import lombok.Data;

@Data
public class TypeDto {

    int slot;

    DescriptionDto type;
}
