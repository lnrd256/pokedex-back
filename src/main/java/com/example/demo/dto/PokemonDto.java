package com.example.demo.dto;

import lombok.Data;

@Data
public class PokemonDto {

    private Integer id;

    private String type;

    private String weight;

    private String name;

    private String ability;
}
