package com.example.demo.dto;

import lombok.Data;

import java.util.List;

@Data
public class BasicPokemonDto {

    private Integer id;

    private String name;

    private String url;

    public Integer weight;

    public List<TypeDto> type;

    public List<AbilityDto> abilityDto;
}
