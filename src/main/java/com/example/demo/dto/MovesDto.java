package com.example.demo.dto;

import lombok.Data;

@Data
public class MovesDto {

    MoveDto move;
}
