package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SpriteDto {

    @JsonProperty("front_default")
    String frontDefault;
}
