package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AbilityDto {

    @JsonProperty("is_hidden")
    Boolean isHidden;

    String slot;

    DescriptionDto ability;
}
