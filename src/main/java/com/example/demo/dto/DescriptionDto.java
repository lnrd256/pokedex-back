package com.example.demo.dto;

import lombok.Data;

@Data
public class DescriptionDto {

    String name;

    String url;
}
