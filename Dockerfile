FROM openjdk:11

RUN mkdir /code
COPY build/libs /code
EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java -jar /code/*.jar" ]
